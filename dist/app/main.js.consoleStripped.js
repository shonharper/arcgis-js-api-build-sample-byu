define("app/main", ["require", "exports", "esri/Map", "esri/views/MapView"], function (require, exports, Map, MapView) {
    "use strict";

    var app = {};
    app.init = function() {
        var map = new Map({
            basemap: "hybrid"
        });
        var view = new MapView({
            map: map,
            container: "viewDiv",
            center: [-111.649311, 40.250693],
            zoom: 19
        });
    }

    var map = new Map({
        basemap: "streets"
    });
    var view = new MapView({
        map: map,
        container: "viewDiv",
        locateByBuildingNumber: 350
    });

    return app;
});