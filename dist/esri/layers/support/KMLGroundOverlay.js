//>>built
define("esri/layers/support/KMLGroundOverlay",["../../core/declare","../../core/lang","./MapImage"],function(a,b,c){return a([c],{declaredClass:"esri.layers.support.KMLGroundOverlay",constructor:function(a){b.isDefined(this.visibility)&&(this.visible=!!this.visibility)}})});
//# sourceMappingURL=KMLGroundOverlay.js.map