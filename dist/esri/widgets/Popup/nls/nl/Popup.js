//>>built
define("esri/widgets/Popup/nls/nl/Popup",{zoom:"Zoomen naar",next:"Volgend object",previous:"Vorig object",close:"Sluiten",dock:"Dok",undock:"Loskoppelen",menu:"Menu",untitled:"Naamloos",pageText:"{index} van {total}",noFeaturesFound:"Geen objecten gevonden",selectedFeature:"Geselecteerd object",selectedFeatures:"{totaal} resultaten",loading:"Laden"});
//# sourceMappingURL=Popup.js.map