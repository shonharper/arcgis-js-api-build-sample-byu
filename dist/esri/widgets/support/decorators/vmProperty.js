//>>built
define("esri/widgets/support/decorators/vmProperty",["require","exports","../../../core/accessorSupport/decorators"],function(f,a,d){Object.defineProperty(a,"__esModule",{value:!0});a.vmProperty=function(a){var b=this;return function(e,c){d.property.call(b,{aliasOf:"viewModel."+(a||c)}).call(b,e,c)}}});
//# sourceMappingURL=vmProperty.js.map