//>>built
define("esri/widgets/ColorPicker/nls/hr/ColorPicker",{transparency:"Prozirnost",suggested:"Predlo\u017eeno",recent:"Nedavno",more:"Vi\u0161e",moreColorsTooltip:"Prika\u017ei vi\u0161e boja.",paletteTooltip:"Odaberi boju.",noColorTooltip:"Nema boje",hexInputTooltip:"Prilago\u0111ena boja u heksadecimalnom zapisu (#FFFF00)."});
//# sourceMappingURL=ColorPicker.js.map