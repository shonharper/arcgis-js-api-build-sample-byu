//>>built
define("esri/widgets/ColorPicker/nls/pt-br/ColorPicker",{transparency:"Transpar\u00eancia",suggested:"Sugerido",recent:"Recente",more:"Mais",moreColorsTooltip:"Visualize mais cores.",paletteTooltip:"Selecione uma cor.",noColorTooltip:"Sem",hexInputTooltip:"Uma cor personalizada na nota\u00e7\u00e3o hexadecimal (#FFFF00)."});
//# sourceMappingURL=ColorPicker.js.map