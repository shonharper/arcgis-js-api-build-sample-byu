//>>built
define("esri/widgets/ColorPicker/nls/it/ColorPicker",{transparency:"Trasparenza",suggested:"Suggerito",recent:"Recente",more:"Maggiori informazioni",moreColorsTooltip:"Visualizza altri colori.",paletteTooltip:"Selezionare un colore.",noColorTooltip:"Nessun colore",hexInputTooltip:"Colore personalizzato nella notazione esadecimale (#FFFF00)."});
//# sourceMappingURL=ColorPicker.js.map