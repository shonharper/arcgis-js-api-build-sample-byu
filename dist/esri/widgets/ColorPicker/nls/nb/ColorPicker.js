//>>built
define("esri/widgets/ColorPicker/nls/nb/ColorPicker",{transparency:"Gjennomsiktighet",suggested:"Foresl\u00e5tt",recent:"Nylig",more:"Mer",moreColorsTooltip:"Vis flere farger.",paletteTooltip:"Velg en farge.",noColorTooltip:"Ingen farge",hexInputTooltip:"En egendefinert farge i form av heksadesimaltegn (#FFFF00)."});
//# sourceMappingURL=ColorPicker.js.map