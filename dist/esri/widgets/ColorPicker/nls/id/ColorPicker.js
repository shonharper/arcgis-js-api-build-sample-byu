//>>built
define("esri/widgets/ColorPicker/nls/id/ColorPicker",{transparency:"Transparansi",suggested:"Disarankan",recent:"Terbaru",more:"Lebih banyak",moreColorsTooltip:"Lihat lebih banyak warna.",paletteTooltip:"Pilih sebuah warna.",noColorTooltip:"Tidak ada warna",hexInputTooltip:"Warna khusus dalam notasi heksadesimal (#FFFF00)."});
//# sourceMappingURL=ColorPicker.js.map