//>>built
define("esri/core/workers/WorkerConnection",["require","exports","./WorkerProxy"],function(f,g,e){return function(){function d(a,b,c){this.id=c;"function"==typeof a?this._target=new a(this):this._target=a;this.proxy=new e(this._target,b,this)}return d.prototype.invoke=function(a,b,c){return this.proxy.invoke(a,b,c,this.id)},d}()});
//# sourceMappingURL=WorkerConnection.js.map