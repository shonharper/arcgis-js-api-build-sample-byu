//>>built
define("esri/core/accessorSupport/decorators/aliasOf",["require","exports","../metadata"],function(e,a,b){Object.defineProperty(a,"__esModule",{value:!0});a.aliasOf=function(a){return function(c,d){b.getPropertyMetadata(c,d).aliasOf=a}}});
//# sourceMappingURL=aliasOf.js.map