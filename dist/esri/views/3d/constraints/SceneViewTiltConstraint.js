//>>built
define("esri/views/3d/constraints/SceneViewTiltConstraint",["../../../core/Accessor","../support/mathUtils"],function(c,d){var b=c.createSubclass([],{declaredClass:"esri.views.3d.constraints.SceneViewTiltConstraint",properties:{mode:{value:"auto"},max:{value:.5,cast:function(a){return d.clamp(a,.5,179.5)},set:function(a){this._set("max",a);this.mode="manual"}}},autoUpdate:function(a){"auto"===this.mode&&this._get("max")!==a&&this._set("max",a)},scale:function(a){}});return b.MAX_DEFAULT=.5,b.MIN_DEFAULT=
179.5,b});
//# sourceMappingURL=SceneViewTiltConstraint.js.map