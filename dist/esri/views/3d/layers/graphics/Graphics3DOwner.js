//>>built
define("esri/views/3d/layers/graphics/Graphics3DOwner",["require","exports"],function(c,a){Object.defineProperty(a,"__esModule",{value:!0});var b=function(){return function(){this.localOriginFactory=this.layerView=this.layer=this.overlaySR=this.renderCoordsHelper=this.renderSpatialReference=this.clippingExtent=this.layerOrderDelta=this.layerOrder=this.stage=this.renderer=this.elevationProvider=this.streamDataSupplier=this.sharedResources=null;this.screenSizePerspectiveEnabled=!0}}();a.Graphics3DSymbolCreationContext=
b});
//# sourceMappingURL=Graphics3DOwner.js.map