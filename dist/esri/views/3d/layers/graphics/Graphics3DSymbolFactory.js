//>>built
define("esri/views/3d/layers/graphics/Graphics3DSymbolFactory",["require","exports","./Graphics3DSymbol","./Graphics3DPointSymbol"],function(g,a,c,d){Object.defineProperty(a,"__esModule",{value:!0});a.make=function(a,e,f){var b;switch(a.type){case "point-symbol-3d":b=d;break;default:b=c}return new b(a,e,f)}});
//# sourceMappingURL=Graphics3DSymbolFactory.js.map