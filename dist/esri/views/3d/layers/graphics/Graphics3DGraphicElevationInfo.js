//>>built
define("esri/views/3d/layers/graphics/Graphics3DGraphicElevationInfo",["require","exports","../../../../core/tsSupport/extendsHelper","./ElevationInfo"],function(f,g,d,e){return function(b){function c(a){a=b.call(this,a)||this;return a.centerPointInElevationSR=null,a}return d(c,b),c}(e)});
//# sourceMappingURL=Graphics3DGraphicElevationInfo.js.map