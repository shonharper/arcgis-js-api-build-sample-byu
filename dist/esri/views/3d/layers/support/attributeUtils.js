//>>built
define("esri/views/3d/layers/support/attributeUtils",["require","exports"],function(g,a){Object.defineProperty(a,"__esModule",{value:!0});a.attributeLookup=function(a,e){for(var f=e.toLowerCase(),b=0,c=Object.keys(a);b<c.length;b++){var d=c[b];if(d.toLowerCase()===f)return a[d]}return null}});
//# sourceMappingURL=attributeUtils.js.map