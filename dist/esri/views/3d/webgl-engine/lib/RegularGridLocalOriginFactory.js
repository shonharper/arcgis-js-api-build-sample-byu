//>>built
define("esri/views/3d/webgl-engine/lib/RegularGridLocalOriginFactory",["require","exports"],function(h,k){var d=function(){function e(a){this._gridSize=a;this._origins={}}return e.prototype.getOrigin=function(a){var c=this._gridSize,d=Math.round(a[0]/c),g=Math.round(a[1]/c);a=Math.round(a[2]/c);var c=""+e.ORIGIN_PREFIX+d+"/"+g+"/"+a,b=this._origins[c];if(!b){var b=this._gridSize,f=e.OFFSET,b={id:c,vec3:[d*b+f,g*b+f,a*b+f]};this._origins[c]=b}return b},e}();return d.ORIGIN_PREFIX="rg_",d.OFFSET=1.11,
d});
//# sourceMappingURL=RegularGridLocalOriginFactory.js.map