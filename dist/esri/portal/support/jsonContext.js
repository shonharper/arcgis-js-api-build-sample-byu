//>>built
define("esri/portal/support/jsonContext",["require","exports","../Portal","../../core/urlUtils"],function(d,a,b,c){Object.defineProperty(a,"__esModule",{value:!0});a.createForItem=function(a){return{origin:"portal-item",url:c.urlToObject(a.itemUrl),portal:a.portal||b.getDefault()}}});
//# sourceMappingURL=jsonContext.js.map