define(
	 "dgrid/extensions/nls/tr/pagination", {
		status: "${start} - ${end} / ${total} sonuçlar",
		gotoFirst: "İlk sayfaya git",
		gotoNext: "Sonraki sayfaya git",
		gotoPrev: "Önceki sayfaya git",
		gotoLast: "Son sayfaya git",
		gotoPage: "Sayfaya git",
		jumpPage: "Sayfaya atla"
	}

);
