define(
"dijit/form/nls/hi/validate", ({
	invalidMessage: "दर्ज की गई संख्या मान्य नहीं है।",
	missingMessage: "यह मान अनिवार्य है.",
	rangeMessage: "यह मान रेंज से बाहर है।"
})
);
